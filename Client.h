#pragma once

#include "CryptoDevice.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <fstream>
#include "hex.h"
#include "md5.h"

#define USERNAME "alex" //the name to login is "alex"
#define PASSWORD "41096F0CBD8A27021F55EAFF9B45EED8"//The password is "SHALOMI" (in large print)
#define DEFAULT_BUFLEN 1024           //length 
#define IP_ADDRESS "127.0.0.1"//local ip
#define DEFAULT_PORT "8202" //server port
using namespace std;

#pragma comment (lib, "Ws2_32.lib")

struct client_type
{
	SOCKET socket;
	int id;
	char received_message[DEFAULT_BUFLEN];
};


int process_client(client_type &new_client);

std::string encryptMD5(std::string str);
