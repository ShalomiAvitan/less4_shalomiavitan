#include "Client.h"
#include <cstdlib>
#include <thread>

using namespace std;
CryptoPP::byte digest[CryptoPP::Weak1::MD5::DIGESTSIZE];

CryptoDevice cryptoDevice;

int process_client(client_type &new_client)
{
	while (1)
	{
		std::memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

			std::string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
			std::string prefix = strMessage.substr(0, position);
			std::string postfix = strMessage.substr(position);
			std::string decrypted_message;

			//this is the only notification we use right now :(
			if (postfix != "Disconnected") {
				//please decrypt this part!
				decrypted_message = postfix;
			}
			else
			{
				//dont decrypt this - not classified and has not been ecrypted! 
				//trying to do so may cause errors
				decrypted_message = postfix;
			}

			if (iResult != SOCKET_ERROR)
			{
				std::cout << prefix + postfix << std::endl;
			}
			else
			{
				std::cout << "recv() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}
		}
	}

	if (::WSAGetLastError() == WSAECONNRESET)
		std::cout << "The server has disconnected" << std::endl;

	return 0;
}

//The function encrypts a string and returns the string encrypted using the MD5 method
std::string encryptMD5(std::string str)
{
	CryptoPP::Weak1::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)str.c_str(), str.length());
	CryptoPP::HexEncoder encoder;
	string output;
	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();
	return output;
}


int main()
{
	string name;
	string password;
	int countTryName = 3; // have 3 choice to enter the name true
	int countTryPass = 3; // have 3 choice to enter the password trur

	bool checkName = false;
	bool checkPass = false;

	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	std::string sent_message = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
	std::string message;

	std::cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		std::cout << "WSAStartup() failed with error: " << iResult << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	std::cout << "Connecting...\n";
	cout << "Register secretly!" << endl;

	while (countTryName != 0 && checkName == false)//the number try on thr name is 3 and in the password.
	{
		cout << "Name: ";
		getline(std::cin, name);//insert fron the user
		if (USERNAME == name)
		{
			checkName == true;
			while (countTryPass != 0 && checkPass == false)
			{
				cout << "Password: ";
				getline(std::cin, password);
				if (PASSWORD == encryptMD5(password))
				{
					checkPass = true;
				}
				else
				{
					countTryPass--;
					if (countTryPass == 0)
					{
						::closesocket(client.socket);
						::WSACleanup();
						std::system("pause");
						return 0;
					}
					cout << "Wrong password - try again" << endl;
				}

			}
		}
		else
		{
			countTryName--;
			if (countTryName == 0)
			{
				::closesocket(client.socket);
				::WSACleanup();
				std::system("pause");
				return 0;
			}
			cout << "Wrong name - try again" << endl;
		}
		if (checkPass)
		{
			break;
		}
	}


	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		std::cout << "getaddrinfo() failed with error: " << iResult << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
			std::cout << "socket() failed with error: " << ::WSAGetLastError() << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		// Connect to server.
		iResult = ::connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
		std::cout << "Unable to connect to server!" << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	std::cout << "Successfully Connected" << std::endl;

	//Obtain id from server for this client;
	::recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		client.id = std::atoi(client.received_message);

		std::thread my_thread(process_client, std::ref(client));

		while (1)
		{
			std::getline(std::cin, sent_message);

			//top secret! please encrypt
			std::string cipher = sent_message;

			iResult = ::send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
				std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
		std::cout << client.received_message << std::endl;

	std::cout << "Shutting down socket..." << std::endl;
	iResult = ::shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		std::cout << "shutdown() failed with error: " << ::WSAGetLastError() << std::endl;
		::closesocket(client.socket);
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	::closesocket(client.socket);
	::WSACleanup();
	std::system("pause");
	return 0;
}